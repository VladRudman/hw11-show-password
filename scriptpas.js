
// "use strick";

let inputs = document.querySelectorAll(".input-wrapper input");
  let icons = document.querySelectorAll(".input-wrapper i");
  let submitButton = document.getElementById("submit-button");
  let errorMessage = document.getElementById("error-message");

  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("focus", function() {
      let icon = this.nextElementSibling;
      icon.classList.toggle("fa-eye");
      icon.classList.toggle("fa-eye-slash");
      this.type = this.type === "password" ? "text" : "password";
    });
  }

  submitButton.addEventListener("click", function(event) {
    event.preventDefault(); 
    let password1 = document.getElementById("password1").value;
    let password2 = document.getElementById("password2").value;
    if (password1 === password2) {
      alert("You are welcome");
    } else {
      errorMessage.innerHTML = "Нужно ввести одинаковые значения";
      errorMessage.style.color = "red";
    }
  });